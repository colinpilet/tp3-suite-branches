import java.util.List;
import java.util.Map;
import java.util.HashMap;
import javax.net.ssl.HostnameVerifier;

public class Box{
    private List<Etudiants> lesEtudiants;
    private Matiere matiere;
    private Utilisateur user;
    private Horaire horaire;
    private Salle salle;

    public Box(Utilisateur user, Matiere matiere, List<Etudiants> lesEtudiants, Horaire horaire){
        this.user = user;
        this.matiere = matiere;
        this.lesEtudiants = lesEtudiants;
        this.horaire = horaire;
        this.salle = new Salle("?", user);
    }

    public void construireSalle(String nom){
        this.salle = new Salle(nom, user);
    }

    public void ajouterEtudiants(Etudiants etu){
        this.lesEtudiants.add(etu);
    }

    public List<Etudiants> getListeEtudiants(){
        return this.lesEtudiants;
    }

    public Matiere getMatiereCours(){
        return this.matiere;
    }

    public Utilisateur getUserCours(){
        return this.user;
    }

    public Horaire getHoraireCours(){
        return this.horaire;
    }


    @Override
    public String toString(){
        return "Les étudiants "+ this.lesEtudiants+" sont en "+ this.matiere;
    }
}