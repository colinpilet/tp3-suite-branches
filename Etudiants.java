import java.util.Map;
import java.util.HashMap;

public class Etudiants {
    private String Nom;
    private String Prenom;
    private int Age;
    private Map<Matiere, Integer> dicoNotes; 

    public Etudiants(String nom, String prenom, int age){
        this.Nom = nom;
        this.Prenom = prenom;
        this.Age = age;
        this.dicoNotes = new HashMap<>();
    } 

    public void ajouterNotes(Matiere matiere, Note note){
        this.dicoNotes.put(matiere, note.getNote());
    }

    public String getNom(){
        return this.Nom;
    }

    public String getPrenom(){
        return this.Prenom;
    }

    public int getAge(){
        return this.Age;
    }

    public int getNoteDico(Matiere mat){
        for(Map.Entry dico : this.dicoNotes.entrySet()){
            if(dico.getKey().equals(mat)){
                return (int) dico.getValue();
            }
        }
        return -1;
    }

    public float getMoyenneEtu(){
        float moy = 0;
        for(Map.Entry dico : this.dicoNotes.entrySet()){
            moy += (int) dico.getValue();
        }
        return moy/this.dicoNotes.size();
    }

    @Override
    public String toString(){
        return "Les notes de " + this.Nom + " " + this.Prenom + " sont : " + this.dicoNotes;
    }
}
