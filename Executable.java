import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Executable {
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        Date dateDebut = simpleDateFormat.parse("18-03-2023 10:00");
        System.out.println(dateDebut);
        Horaire h1 = new Horaire("18-03-2023 10:00", "18-03-2023 11:00");
        System.out.println(h1);
    }
}
