public class ExecutableEtudiants {
    public static void main(String[] args){
        Matiere francais = Matiere.FRANCAIS;
        Matiere maths = Matiere.MATHEMATIQUE;
        Note n1 = new Note(10, francais);
        Note n2 = new Note(20, francais);
        Note n3 = new Note(15, maths);
        assert n1.getNote() == 10;
        assert n2.getNote() == 20;
        assert n3.getNote() == 15;
        Etudiants colin = new Etudiants("Pilet", "Colin", 18);
        assert colin.getAge() == 18;
        assert colin.getNom() == "Pilet";
        assert colin.getPrenom() == "Colin";
        colin.ajouterNotes(francais, n1);
        colin.ajouterNotes(francais, n2);
        colin.ajouterNotes(maths, n3);
        assert colin.getNoteDico(maths) == 15;
        assert colin.getMoyenneEtu() == 17.5;
    }
}
