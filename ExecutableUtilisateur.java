import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;


public class ExecutableUtilisateur {
    public static void main(String[] args) throws ParseException{
        Utilisateur u1 = new Utilisateur("Gérard", "Dubois", 5);
        Salle s1 = new Salle("202", u1);
        Matiere francais = Matiere.FRANCAIS;
        Matiere maths = Matiere.MATHEMATIQUE;
        Matiere anglais = Matiere.ANGLAIS;
        Matiere sport = Matiere.SPORT;
        Note n1 = new Note(14, anglais);
        Note n2 = new Note(20, francais);
        Note n3 = new Note(15, maths);
        Note n4 = new Note(12, sport);
        Etudiants colin = new Etudiants("Pilet", "Colin", 18);
        Etudiants leo = new Etudiants("Lucidor", "Léo", 18);
        colin.ajouterNotes(anglais, n1);
        colin.ajouterNotes(francais, n2);
        colin.ajouterNotes(maths, n3);
        leo.ajouterNotes(francais, n2);
        leo.ajouterNotes(maths, n3);
        leo.ajouterNotes(sport, n4);
        assert colin.getNoteDico(maths) == 15;
        Horaire h1 = new Horaire("18-02-2023 15:00", "18-02-2023 16:00");
        Horaire h2 = new Horaire("18-02-2023 16:00", "18-02-2023 17:00");
        List<Etudiants> listeEtu1 = new ArrayList<>(Arrays.asList(leo, colin));
        Box cour1 = new Box(u1, maths, listeEtu1, h1);
        Box cour2 = new Box(u1, francais, listeEtu1, h2);
        cour1.construireSalle(s1.getNom());
        u1.ajouterCours(cour1);
        u1.ajouterCours(cour2);
        assert u1.getMoyenneMatiere(francais) == 20.0;
        assert u1.getMoyennePromo() == 16.0;
    }
}