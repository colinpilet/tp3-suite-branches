import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Horaire {
    private Date debut;
    private Date fin;

    public Horaire(String debut, String fin) throws ParseException{
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        Date dateDebut = simpleDateFormat.parse(debut);
        Date dateFin = simpleDateFormat.parse(fin);
        this.debut = dateDebut;
        this.fin = dateFin;
    }

    public Date getDebut(){
        return this.debut;
    }

    public Date getFin(){
        return this.fin;
    }

    
    @Override
    public String toString(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        return "Le cour commence à "+simpleDateFormat.format(debut)+" et fini à "+simpleDateFormat.format(fin);
    }
}