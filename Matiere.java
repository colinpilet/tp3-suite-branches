public enum Matiere {
    MATHEMATIQUE,
    FRANCAIS,
    ANGLAIS,
    SPORT;

    @Override
    public String toString(){
        if (this == MATHEMATIQUE){
            return "Mathematique";
        }
        else if (this == FRANCAIS){
            return "Français";
        }
        else if (this == ANGLAIS){
            return "Anglais";
        }
        else if (this == SPORT){
            return "Sport";
        }
        return "La matière n'existe pas";
    }
}

