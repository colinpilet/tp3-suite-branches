public class Note {
    private Integer note;
    private Matiere matiere;
    
    
    public Note(Integer note, Matiere matiere){
        this.note = note;
        this.matiere = matiere;
    }

    public Integer getNote(){
        return this.note;
    }

    public Matiere getMatiere(){
        return this.matiere;
    }
}
