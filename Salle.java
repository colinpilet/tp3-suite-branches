public class Salle {
    private String nom;
    private Utilisateur user;

    public Salle(String nom, Utilisateur user){
        this.nom = nom;
        this.user = user;
    }

    public String getNom(){
        return this.nom;
    }

    public String toString(){
        return "La salle N°" + this.nom;
    }
}
