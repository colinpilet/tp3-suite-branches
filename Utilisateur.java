import java.util.List;
import java.util.ArrayList;

public class Utilisateur {
    private String nom;
    private String prenom;
    private int idUtilisateur;
    private List<Box> lesCours;
    
    
    public Utilisateur(String prenom, String nom, int id){
        this.nom = nom;
        this.prenom = prenom;
        this.idUtilisateur = id;
        this.lesCours = new ArrayList<>();
    }

    public void ajouterCours(Box cour){
        this.lesCours.add(cour);
    }

    public float getMoyenneMatiere(Matiere matiere){
        float moy = 0;
        int nbNote = 0; 
        for(Box cour : this.lesCours){
            for(Etudiants etu : cour.getListeEtudiants()){
                if(etu.getNoteDico(matiere) != -1){
                    moy += etu.getNoteDico(matiere);
                    nbNote += 1;
                }
            }
        }
        return moy/nbNote;
    }

    public float getMoyennePromo(){
        int nbEtu = 0;
        float addMoyenne = 0;
        for(Box cour : this.lesCours){
            for(Etudiants etu : cour.getListeEtudiants()){
                addMoyenne += etu.getMoyenneEtu();
                nbEtu += 1;
            }
        }
        return addMoyenne/nbEtu;
    }

    public String toString(){
        return "nom : " + this.nom + ", prénom : " + this.prenom + ", id : " + this.idUtilisateur;
    }
}